﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace compare2Values
{
    class Program
    {
        static void Main(string[] args)
        {
            var num1 = 3;
            var num2 = 5;

            if (num1 > num2)
            {
                Console.WriteLine($"{num1} > {num2}.");
            } else if (num1 <= num2)
            {
                Console.WriteLine($"\n{num1} <= {num2}.");
            } // End loop If..Else
        }
    } // End class Program
}
